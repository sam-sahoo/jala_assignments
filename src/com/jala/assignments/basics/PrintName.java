package com.jala.assignments.basics;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class PrintName {
    public static void main(String[] args){
        //Get the name
        try{
            //The name is fetched here and displayed
            String name = fetchName();
            System.out.println( "The entered name is: " + name );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static String fetchName() throws IOException {
        //Enter your name
        BufferedReader reader = new BufferedReader(new BufferedReader(new InputStreamReader(System.in)));
        String name = reader.readLine();
        return name;
    }
}
